package com.samsen.lifesim;

import java.util.Arrays;
import java.util.Random;

public class Display {
    private char[][] field;
    private int width;
    private int hight;

    public Display(int width, int hight) {
        this.width = width;
        this.hight = hight;
        field = new char[width][hight];
        generate();
    }

    private void generate(){
        /*
        * куст
        ~ вода
        # земля
        + трава
        */
        int aquaPercent = 30;
        int grassPercent = 40;
        int bushPercent = 10;

        int area = getHight()*getWidth();
        int aquaArea = (area*aquaPercent)/100;
        int grassArea = (area*grassPercent)/100;
        int bushArea = (area*bushPercent)/100;



        for (int i = 0; i < getHight()-1; i++) {
            for (int j = 0; j < getWidth()-1; j++){
                field[j][i] = '#';
            }
        }

        for (int i = 0; i < bushArea ; i++) {
            Random rand = new Random();
            int x =  rand.nextInt(getWidth());
            int y = rand.nextInt(getHight());
            field[x][y] = '*';
        }
    }

    private int getWidth() {
        return width;
    }

    private int getHight(){
        return hight;
    }

    public void show(){
        for (int i = 0; i < getHight()-1; i++) {
            for (int j = 0; j < getWidth()-1; j++){
                System.out.print(field[j][i]);
            }
            System.out.println();
        }
    }

}
